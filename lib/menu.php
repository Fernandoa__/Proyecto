<div class="nav-fixed">
  <nav class="grey lighten-4">
  <a href="Index.php" class="brand-logo center grey-text text-grey darken-2"><img src="../img/Logo.png" class="mediana"></a>
    
 
    </nav>
  </div>
 
<div class='navbar-wrapper'>
		<nav class='nav-extended grey lighten-4'>
			<div class='nav-wrapper'>
				<a  href='#' data-activates='mobile' class='button-collapse'><i class='material-icons pink-text'>menu</i></a>
				<ul class='left hide-on-med-and-down'>
				<li><a class="indigo-text darken-2"       href='empresa.php'><i class='material-icons left'>dashboard</i><b>Empresa</b></a></li>
				<li><a class="red-text"                   href='productos.php'><i class='material-icons left'>shopping_basket</i><b>Productos</b></a></li>
				<li><a class="orange-text accent-3"       href='servicios.php'>  <i class='material-icons left'>toc</i><b>Servicios</b></a></li>
        <li><a class="light-green-text accent-3"  href='contactos.php'>  <i class='material-icons left'>contacts</i><b>Contactos</b></a></li>
				<li><a class="orange-text accent-3"       href='iniciar_sesion.php'>  <i class='material-icons left'>person</i><b>Acceder a mi cuenta</b></a></li>
				</ul>
			</div>
		</nav>
	</div>
	<!-- Menú lateral para dispositivos móviles -->
	<ul class='side-nav' id='mobile'>
      	<li><a class="indigo-text darken-2"       href='#productos'><i class='material-icons left'>dashboard</i><b>Empresa</b></a></li>
				<li><a class="red-text"                   href='#'>         <i class='material-icons left'>shopping_basket</i><b>Productos</b></a></li>
				<li><a class="orange-text accent-3"       href='#acceder'>  <i class='material-icons left'>toc</i><b>Servicios</b></a></li>
        <li><a class="light-green-text accent-3"  href='#acceder'>  <i class='material-icons left'>contacts</i><b>Contactos</b></a></li>
	</ul>
 <?php

?>
     </ul>
    </div>
  </nav>