<footer class="page-footer black">
          <div class="container">
            <div class="row">
              <div class="col l6 s12">
                <h5 class="red-text text-lighten-1">TERMS OF SERVICE | PRIVACY POLICY</h5>
                <p class="grey-text text-lighten-4">© INDUSTRIAS ARIARTI 2017 ALL RIGHTS RESERVED.</p>
              </div>
              <div class="col l4 offset-l2 s12">
                <h5 class="red-text text-lighten-1">Contactenos</h5>
                <ul>
                  <li><a class="grey-text text-lighten-3" href="#!">Telefono 25169066</a></li>
                  <li><a class="grey-text text-lighten-3" href="#!">Direccion Col Miralvalle Av Florencia No 40</a></li>
                  <li><a class="grey-text text-lighten-3" href="#!">Email industriasariarti@gmail.com</a></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="footer-copyright">
            <div class="container">
            © 2014 Copyright |ARIARTI  
            <a class="grey-text text-lighten-4 right" href="#!"></a>
            </div>
          </div>
        </footer>
