<?php
    //primeros "importamos el archivo de la conexion"
    require("../../lib/conexion.php");
    require("../../lib/validator.php");


    if(empty($_GET['id_tipo_diseño'])) 
    {
        Page::header("Agregar Tipo Diseño");
        $id_tipo_diseño = null;
        $nombre_diseño = null;
    }
    else
    {
        Page::header("Modificar Tipo Diseño");
        $id = $_GET['id'];
        $sql = "SELECT * FROM tipo_diseño WHERE id_tipo_diseño = ?";
        $params = array($id);
        $data = Database::getRow($sql, $params);
        $nombre = $data['nombre_diseño'];
    }

        if(!empty($_POST))
        {
            $_POST = Validator::validateForm($_POST);
            $nombre = $_POST['nombre_diseño'];

            if($nombre == "")
            {
                $nombre = null;
            }

            try 
            {
                if($nombre != "")
                {
                    if($id == null)
                    {
                        $sql = "INSERT INTO tipo_diseño(nombre_diseño) VALUES(?)";
                        $params = array($nombre);
                    }
                    else
                    {
                        $sql = "UPDATE tipo_diseño SET nombre_diseño = ? WHERE id_tipo_diseño = ?";
                        $params = array($nombre, $descripcion, $id);
                    }
                    if(Database::executeRow($sql, $params))
                    {
                        Page::showMessage(1, "Operación satisfactoria", "index.php");
                    }
                    else
                    {
                        throw new Exception("Operación fallida");
                    }
                }
                else
                {
                    throw new Exception("Debe digitar un nombre");
                }
            }
            catch (Exception $error)
            {
                Page::showMessage(2, $error->getMessage(), null);
            }
    }
?>