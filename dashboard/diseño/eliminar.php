<?php
require("../lib/page.php");
Page::header("Eliminar Tipo Diseño");

if(!empty($_GET['id']))
{
    $id = $_GET['id'];
}
else
{
    header("location: index.php");
}

if(!empty($_POST))
{
	$id = $_POST['id'];
	try 
	{
		$sql = "DELETE FROM tipo_diseño WHERE id_tipo_diseño = ?";
	    $params = array($id);
	    if(Database::executeRow($sql, $params))
		{
			Page::showMessage(1, "Operación satisfactoria", "index.php");
		}
		else
		{
			throw new Exception("Operación fallida");
		}
	} 
	catch (Exception $error) 
	{
		Page::showMessage(2, $error->getMessage(), "index.php");
	}
}
?>