<?php
require("../page/designe.php");
Page::header("cliente");

$id         = base64_decode($_GET['id']);
$sql        = "SELECT * FROM cliente WHERE id_cliente = ?";
$params     = array(
  $id
);
$data       = Database::getRow($sql, $params);
$nombres    = $data['nombre_cliente'];
$apellidos  = $data['apellido_cliente'];
$correo     = $data['correo'];
$presupuesto = $data['id_presupuesto'];

?>
 

<form class="container" method="post" enctype='multipart/form-data'>
    <div class="row">
        <h2 class="center-align">Cliente</h2>
    </div>
    <div class='row'>
        <div class='input-field col s12 m6'>
              <i class='material-icons prefix'>person</i>
              <input id='nombre_cliente' type='text' name='nombre' class='validate' disabled="" value="<?php
print($nombre);
?>">
              <label for='nombres'>Nombres</label>
        </div>
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>person</i>
            <input id='apellido_cliente' type='text' name='apellido_cliente' class='validate' disabled="" value="<?php
print($apellidos);
?>">
            <label for='apellidos'>Apellidos</label>
        </div>
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>assignment_ind</i>
            <input id='apellidos' type='text' name='dui' class='validate' disabled="" value="<?php
print($correo);
?>">
            <label for='correo'>Correo</label>
        </div>
        <div class="input-field col s12">
          <i class="material-icons prefix">mode_edit</i>
          <textarea id="icon_prefix2" class="materialize-textarea" name="residencia" disabled=""><?php
print($presupuesto);
?></textarea>
          <label for="icon_prefix2">Presupuesto</label>
        </div>
    </div>
    <div class='row center-align'>
         <a class='btn waves-effect black' href="menux.php"><i class='material-icons left'>replay</i>Regresar</a>
    </div>
    <br>
</form>

<?php
Page::footer();
?>