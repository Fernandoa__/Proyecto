<!-- Se solicita el archivo maestro donde esta la configuracion de la pagina -->
<?php
require("../page/page.php");
if(empty($_GET['id'])) 
{
    Page::header("Agregar Cliente");
    //Se declaran la variables
    $id = null;
    $nombres = null;
    $apellidos = null;
    $correo = null;
    $alias = null;
    $foto = null;

}
else
{
    Page::header("Modificar usuario");
    //Se obtiene el Id del dato guardado
    $id = $_GET['id'];
    $sql = "SELECT * FROM cliente WHERE id_cliente = ?";
    $params = array($id);
    //Se obtienen los datos de todas la columnas
    $data = Database::getRow($sql, $params);
    $nombres = $data['nombre_cliente'];
    $apellidos = $data['apellido_cliente'];
    $correo = $data['correo'];
    $alias = $data['alias'];
}

if(!empty($_POST))
{
    //Se validan los datos
    $_POST = Validator::validateForm($_POST);
  	$nombres = $_POST['nombres'];
  	$apellidos = $_POST['apellidos'];
    $correo = $_POST['correo'];
    $alias = $_POST['alias'];

    try 
    {
        //En caso de que los campos esten vacios no permitira que se guarde
      	if($nombres != "" && $apellidos != "")
        {
            if($correo != "")
            {
                if($id == null)
                {
                    $alias = $_POST['alias'];
                    if($alias != "")
                    {
                        $clave1 = $_POST['clave1'];
                        $clave2 = $_POST['clave2'];
                        if($clave1 != "" && $clave2 != "")
                        {
                            if($clave1 == $clave2)
                            {
                                $clave = password_hash($clave1, PASSWORD_DEFAULT);
                                $sql = "INSERT INTO cliente(nombre_cliente, apellido_cliente, correo, alias, clave) VALUES(?, ?, ?, ?, ?)";
                                $params = array($nombres, $apellidos, $correo, $alias, $clave);
                            }
                            else
                            {
                                throw new Exception("Las contraseñas no coinciden");
                            }
                        }
                        else
                        {
                            throw new Exception("Debe ingresar ambas contraseñas");
                        }
                    }
                    else
                    {
                        throw new Exception("Debe ingresar un alias");
                    }
                            }
                            else
                           {
                    $sql = "UPDATE cliente SET nombre_cliente = ?, apellido_cliente = ?, correo = ? WHERE id_cliente = ?";
                    $params = array($nombres, $apellidos, $correo, $id);
                    }
                Database::executeRow($sql, $params);
                header("location: index.php");
            }
            else
            {
                throw new Exception("Debe ingresar un correo electrónico");
            }
        }
        else
        {
            throw new Exception("Debe ingresar el nombre completo");
        }
    }
    catch (Exception $error)
    {
        Page::showMessage(2, $error->getMessage(), null);
    }
}
?>

<!-- Botones y cuadros de texto -->
<form method='post'>
    <div class='row'>
        <div class='input-field col s12 m6'>
          	<i class='material-icons prefix'>person</i>
          	<input id='nombres' type='text' name='nombres' class='validate' value='<?php print($nombres); ?>' required/>
          	<label for='nombres'>Nombres</label>
        </div>
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>person</i>
            <input id='apellidos' type='text' name='apellidos' class='validate' value='<?php print($apellidos); ?>' required/>
            <label for='apellidos'>Apellidos</label>
        </div>
    </div>
    <div class='row'>
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>email</i>
            <input id='correo' type='email' name='correo' class='validate' value='<?php print($correo); ?>' required/>
            <label for='correo'>Correo</label>
        </div>
         <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>person_pin</i>
            <input id='alias' type='text' name='alias' class='validate' <?php print("value='$alias' "); print(($id == null)?"required":"disabled"); ?>/>
            <label for='alias'>Alias</label>
        </div>
        </div>
    <?php
    if($id == null)
    {
    ?>
    <div class='row'>
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>security</i>
            <input id='clave1' type='password' name='clave1' class='validate' required/>
            <label for='clave1'>Contraseña</label>
        </div>
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>security</i>
            <input id='clave2' type='password' name='clave2' class='validate' required/>
            <label for='clave2'>Confirmar contraseña</label>
        </div>
    </div>
    <?php
    }
    ?>
    <div class='row center-align'>
        <a href='index.php' class='btn waves-effect grey'><i class='material-icons'>cancel</i></a>
        <button type='submit' class='btn waves-effect blue'><i class='material-icons'>save</i></button>
    </div>
</form>

<?php
Page::footer();
?>