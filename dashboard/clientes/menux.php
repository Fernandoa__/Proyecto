<?php
require("../page/designe.php");
Page::header("clientes");
?>
<form method='post'>
<div class='row'>
    <div class='input-field col s6 m4'>
        <i class='material-icons prefix'>search</i>
        <input id='buscar' type='text' name='buscar'/>
        <label for='buscar'>Buscar</label>
    </div>
<div class='input-field col s6 m4'>
<button type='submit' class='btn waves-effect green darken-3'><i class='material-icons'>check_circle</i></button>     
</div>
</div>
    </form>

<?php

if (!empty($_POST)) {
  $search = trim($_POST['buscar']);
  $sql    = "SELECT * FROM cliente WHERE id_cliente LIKE ? ORDER BY id_cliente";
  $params = array(
    "%$search%"
  );
} else {
  $sql    = "SELECT * FROM cliente ORDER BY id_cliente";
  $params = null;
}
$data = Database::getRows($sql, $params);
if ($data != null) {
?>
<table class='striped'>
<thead>
    <tr>
    <th>IMAGEN</th>
    <th>NOMBRE</th>
    <th>APELLIDO</th>

</tr>
    </thead>
    <tbody>

<?php
  foreach ($data as $row) {
    print("
            <tr>
                <td><img src='data:image/*;base64," . $row['foto'] . "' class='materialboxed' width='100' height='100'></td>
                <td>" . $row['nombres'] . "</td>
                <td>" . $row['apellidos'] . "</td>
                
        ");
    print("
            <td>
                    <a href='view.php?id=" . base64_encode($row['id_cliente']) . "' class='blue-text'><i class='material-icons'>visibility</i></a>
                </td>");
    print("
            </tr>
        ");
  }
  print("
        </tbody>
    </table>
    ");
} //Fin de if que comprueba la existencia de registros.
else {
  print("<div class='card-panel yellow'><i class='material-icons left'>warning</i>No hay registros!</div>");
}
Page::footer();
?>
