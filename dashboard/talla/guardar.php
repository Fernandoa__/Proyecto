<?php
    //primeros "importamos el archivo de la conexion"
    require("../page/page.php");

    if(empty($_GET['id_talla'])) 
    {
        Page::header("Agregar Tipo Diseño");
        $nombre = null;
    }
    else
    {
        Page::header("Modificar Tipo Diseño");
        $id = $_GET['id'];
        $sql = "SELECT * FROM talla WHERE id_talla = ?";
        $params = array($id);
        $data = Database::getRow($sql, $params);
        $nombre = $data['talla'];
    }

        if(!empty($_POST))
        {
            $_POST = Validator::validateForm($_POST);
            $nombre = $_POST['nombre'];
            try 
            {
                if($nombre != "")
                {
                    if($id == null)
                    {
                        $sql = "INSERT INTO talla(talla) VALUES(?)";
                        $params = array($nombre);
                    }
                    else
                    {
                        $sql = "UPDATE talla SET talla = ? WHERE id_talla = ?";
                        $params = array($nombre, $id);
                    }
                    if(Database::executeRow($sql, $params))
                    {
                        Page::showMessage(1, "Operación satisfactoria", "index.php");
                    }
                    else
                    {
                        throw new Exception("Operación fallida");
                    }
                }
                else
                {
                    throw new Exception("Debe digitar un nombre");
                }
            }
            catch (Exception $error)
            {
                Page::showMessage(2, $error->getMessage(), null);
            }
    }
?>

<!-- Botones y cuadros de texto -->
<form method='post'>
    <div class='row'>
        <div class='input-field col s12 m12'>
            <i class='material-icons prefix'>note_add</i>
            <input id='nombre' type='text' name='nombre' class='validate' value='<?php print($nombre); ?>' required/>
            <label for='nombre'>Talla</label>
        </div>
    </div>
    <div class='row center-align'>
        <a href='index.php' class='btn waves-effect grey'><i class='material-icons'>cancel</i></a>
        <button type='submit' class='btn waves-effect blue'><i class='material-icons'>save</i></button>
    </div>
</form>

<?php
Page::footer();
?>