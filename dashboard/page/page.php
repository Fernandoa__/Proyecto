<?php
require("../../lib/database.php");
require("../../lib/validator.php");
class Page
{
	public static function header($title)
	{
		session_start();
		ini_set("date.timezone","America/El_Salvador");
		print("
			<!DOCTYPE html>
      <html lang ='es'>
      <head>
				<meta charset='utf-8'>
				<title>Dashboard - .$title.</title>
				<meta charset='utf-8'>
        <link type='text/css' rel='stylesheet' href='../../css/materialize.min.css'/>
	      <link type='text/css' rel='stylesheet' href='../../css/icons.css'/>
        <link href='../../css/miestilo.css' type='text/css' rel='stylesheet' media='screen,projection'/>
				<link type='text/css' rel='stylesheet' href='../../css/sweetalert2.min.css'/>
				<link type='text/css' rel='stylesheet' href='../css/dashboard.css'/>
				<script type='text/javascript' src='../../js/sweetalert2.min.js'></script>
				<meta name='viewport' content='width=device-width, initial-scale=1.0'/>
			</head>
			<body>
		");
		if(isset($_SESSION['nombre_usuario']))
		{
			print("
			<div class='nav-fixed'>
        <nav class='grey lighten-4'>
        <a href='index.php' class='brand-logo center grey-text text-grey darken-2'><img src='../../img/Logo.png' class='pequeña'></a>
    
 
        </nav>
      </div>
 
      <div class='navbar-wrapper'>
          <nav class='nav-extended grey lighten-4'>
            <div class='nav-wrapper'>
              <a  href='#' data-activates='mobile' class='button-collapse'><i class='material-icons pink-text'>menu</i></a>
              <ul class='left hide-on-med-and-down'>
              <li><a class='indigo-text darken-2''       href='../usuarios/'><i class='material-icons left'>dashboard</i><b>Usuarios</b></a></li>
              <li><a class='red-text'                   href='../clientes/'><i class='material-icons left'>shopping_basket</i><b>Clientes</b></a></li>
              <li><a class='orange-text accent-3'       href='../tipo_producto/'>  <i class='material-icons left'>toc</i><b>Tipo Producto</b></a></li>
              <li><a class='light-green-text accent-3''  href='../productos/'>  <i class='material-icons left'>contacts</i><b>Productos</b></a></li>
              <li><a class=' lime-text accent-1'       href='../talla/'>  <i class='material-icons left'>turned_in_not</i><b>Talla</b></a></li>
							<li><a class='deep-orange-text lighten-2'       href='../diseño/'>  <i class='material-icons left'>polymer</i><b>Diseño</b></a></li>
							<li><a class='orange-text accent-3'       href='../main/logout.php'>  <i class='material-icons left'>person</i><b>Salir</b></a></li>
              </ul>
            </div>
          </nav>
        </div>
        <!-- Menú lateral para dispositivos móviles -->
        <ul class='side-nav' id='mobile'>
              <li><a class='indigo-text darken-2'       href='#productos'><i class='material-icons left'>dashboard</i><b>Empresa</b></a></li>
              <li><a class='red-text''                   href='#'>         <i class='material-icons left'>shopping_basket</i><b>Productos</b></a></li>
              <li><a class='orange-text accent-3'       href='#acceder'>  <i class='material-icons left'>toc</i><b>Servicios</b></a></li>
              <li><a class='light-green-text accent-3'  href='#acceder'>  <i class='material-icons left'>contacts</i><b>Contactos</b></a></li>
        </ul>
				<ul id='dropdown-mobile' class='dropdown-content'>
					<li><a href='../main/principal.php'>Editar perfil</a></li>
					<li><a href='../main/logout.php'>Salir</a></li>
				</ul>
				<main class='container'>
					<h3 class='center-align'>.$title.</h3>
			");
		}
		else
		{
			print("
				<header class='navbar-fixed'>
					<nav class='blue-grey lighten-3'>
						<div class='nav-wrapper'>
							<a href='../main/' class='brand-logo'><i class='material-icons'>dashboard</i></a>
						</div>
					</nav>
				</header>
				<main class='container'>
			");
			$filename = basename($_SERVER['PHP_SELF']);
			if($filename != "login.php" && $filename != "registrar.php")
			{
				self::showMessage(3, "¡Debe iniciar sesión!", "../main/login.php");
				self::footer();
				exit;
			}
			else
			{
				print("<h3 class='center-align'>".$title."</h3>");
			}
		}
	}

	public static function footer()
	{
		print("
			<footer class='page-footer black'>
          <div class='container'>
            <div class='row'>
              <div class='col l6 s12'>
                <h5 class='red-text text-lighten-1'>TERMS OF SERVICE | PRIVACY POLICY</h5>
                <p class='grey-text text-lighten-4'>© INDUSTRIAS ARIARTI 2017 ALL RIGHTS RESERVED.</p>
              </div>
              <div class='col l4 offset-l2 s12'>
                <h5 class='red-text text-lighten-1'>Contactenos</h5>
                <ul>
                  <li><a class='grey-text text-lighten-3' href='#!'>Telefono 25169066</a></li>
                  <li><a class='grey-text text-lighten-3' href='#!'>Direccion Col Miralvalle Av Florencia No 40</a></li>
                  <li><a class='grey-text text-lighten-3' href='#!'>Email industriasariarti@gmail.com</a></li>
                </ul>
              </div>
            </div>
          </div>
          <div class='footer-copyright'>
            <div class='container'>
            © 2014 Copyright |ARIARTI  
            <a class='grey-text text-lighten-4 right' href='#!'></a>
            </div>
          </div>
        </footer>
			<script type='text/javascript' src='../../js/jquery-2.1.1.min.js'></script>
			<script type='text/javascript' src='../../js/materialize.min.js'></script>
			<script type='text/javascript' src='../js/dashboard.js'></script>
			</body>
			</html>
		");
	}

	public static function setCombo($label, $name, $value, $query)
	{
		$data = Database::getRows($query, null);
		print("<select name='$name' required>");
		if($data != null)
		{
			if($value == null)
			{
				print("<option value='' disabled selected>Seleccione una opción</option>");
			}
			foreach($data as $row)
			{
				if(isset($_POST[$name]) == $row[0] || $value == $row[0])
				{
					print("<option value='$row[0]' selected>$row[1]</option>");
				}
				else
				{
					print("<option value='$row[0]'>$row[1]</option>");
				}
			}
		}
		else
		{
			print("<option value='' disabled selected>No hay registros</option>");
		}
		print("
			</select>
			<label>$label</label>
		");
	}

	public static function showMessage($type, $message, $url)
	{
		$text = addslashes($message);
		switch($type)
		{
			case 1:
				$title = "Éxito";
				$icon = "success";
				break;
			case 2:
				$title = "Error";
				$icon = "error";
				break;
			case 3:
				$title = "Advertencia";
				$icon = "warning";
				break;
			case 4:
				$title = "Aviso";
				$icon = "info";
		}
		if($url != null)
		{
			print("<script>swal({title: '$title', text: '$text', type: '$icon', confirmButtonText: 'Aceptar', allowOutsideClick: false, allowEscapeKey: false}).then(function(){location.href = '$url'})</script>");
		}
		else
		{
			print("<script>swal({title: '$title', text: '$text', type: '$icon', confirmButtonText: 'Aceptar', allowOutsideClick: false, allowEscapeKey: false})</script>");
		}
	}
}
?>