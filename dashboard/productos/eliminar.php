<!-- Se solicita el archivo maestro donde esta la configuracion de la pagina -->
<?php
require("../page/page.php");
// Se coloca el titulo
Page::header("Eliminar producto");

if(!empty($_GET['id'])) 
{
    $id = $_GET['id'];
}
else
{
    header("location: index.php");
}

if(!empty($_POST))
{
	$id = $_POST['id'];
	try 
	{
		//Si esta con sesion abierta no se puede eliminar a el mismo
		$sql = "DELETE FROM productos WHERE id_producto = ?";
	    $params = array($id);
	    Database::executeRow($sql, $params);
	    header("location: index.php");
	}
	catch (Exception $error) 
	{
		Page::showMessage(2, $error->getMessage(), "index.php");
	}
}
?>
<!--Alerta-->
<form method='post'>
	<div class='row center-align'>
		<input type='hidden' name='id' value='<?php print($id); ?>'/>
		<button type='submit' class='btn waves-effect green'><i class='material-icons'>check_circle</i></button>
		<a href='index.php' class='btn waves-effect grey'><i class='material-icons'>cancel</i></a>
	</div>
</form>

<?php
Page::footer();
?>