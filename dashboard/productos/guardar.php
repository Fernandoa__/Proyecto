<!-- Se solicita el archivo maestro donde esta la configuracion de la pagina -->
<?php
require("../page/page.php");
if(empty($_GET['id'])) 
{
    Page::header("Agregar producto");
    //Se declaran la variables
    $id = null;
    $nombre = null;
    $color = null;
    $categoria = null;
    $precio = null;
    $imagen = null;
    $estado = 1;
    $descripcion = null;
    $tipo= null;
    $talla = null;

}
else
{
    Page::header("Modificar producto");
    //Se obtiene el Id del dato guardado
    $id = $_GET['id'];
    $sql = "SELECT * FROM productos WHERE id_producto = ?";
    $params = array($id);
    //Se obtienen los datos de todas la columnas
    $data = Database::getRow($sql, $params);
    $nombre = $data['nombre'];
    $descripcion = $data['descripcion'];
    $imagen = $data['imagen'];
    $estado = $data['estado'];
    $color = $data['color'];
    $tipo = $data['tipo'];
    $talla = $data['talla'];
    
}

if(!empty($_POST))
{
    //Se validan los datos
    $_POST = Validator::validateForm($_POST);
  	$nombre = $_POST['nombre'];
    $descripcion = $_POST['descripcion'];
    $archivo = $_FILES['imagen'];
    $estado = $_POST['estado'];
    $color = $_POST['color'];
    $tipo = $_POST['tipo'];
    $talla = $_POST['talla'];

    try 
    {
        //En caso de que los campos esten vacios no permitira que se guarde
        if($nombre != "")
        {
            if($descripcion != "")
            {              
                 if($archivo['name'] != null)
                            { 
                                //Se valida la imagen
                                $base64 = Validator::validateImage($archivo);
                                if($base64 != false)
                                {
                                    $imagen = $base64;
                                }
                                else
                                {
                                    throw new Exception("Ocurrió un problema con la imagen");
                                }
                            }
                            else
                            {
                                if($imagen == null)
                                {
                                    throw new Exception("Debe seleccionar una imagen");
                                }
                            }
                            if($id == null)
                            {
                                $sql = "INSERT INTO productos(nombre_producto, descripcion_producto, imagen, estado_producto, id_colores, id_tipo_producto, id_talla) VALUES(?, ?, ?, ?, ?, ?, ?)";
                                $params = array($nombre, $descripcion, $imagen, $estado, $color, $tipo, $talla);
                            }
                            else
                            {
                                $sql = "UPDATE productos SET nombre_producto = ?, descripcion_producto = ?, imagen = ?, estado_producto = ?, id_colores = ?, id_tipo_producto = ?, id_talla = ? WHERE id_producto = ?";
                                $params = array($nombre, $descripcion, $imagen, $estado, $color, $tipo, $talla, $id);
                            }
                            Database::executeRow($sql, $params);
                            header("location: index.php");
                                          
          }
    else
    {
            throw new Exception("Debe digitar descripcion");
    }
          }
    else
    {
            throw new Exception("Debe digitar el nombre");
    }
}
    catch (Exception $error)
    {
        Page::showMessage(2, $error->getMessage(), null);
    }
}
?>

<!-- Botones y cuadros de texto -->
<form method='post' enctype='multipart/form-data'>
    <div class='row'>
        <div class='input-field col s12 m6'>
          	<i class='material-icons prefix'>note_add</i>
          	<input id='nombre' type='text' name='nombre' class='validate' value='<?php print($nombre); ?>' required/>
          	<label for='nombre'>Nombre</label>
        </div>
        <div class='input-field col s12 m6'>
          	<i class='material-icons prefix'>description</i>
          	<input id='descripcion' type='text' name='descripcion' class='validate' value='<?php print($descripcion); ?>'/>
          	<label for='descripcion'>Descripción</label>
        </div>
        <div class='file-field input-field col s12 m6'>
            <div class='btn waves-effect'>
                <span><i class='material-icons'>image</i></span>
                <input type='file' name='imagen' <?php print(($imagen == null)?"required":""); ?>/>
            </div>
            <div class='file-path-wrapper'>
                <input class='file-path validate' type='text' placeholder='Seleccione una imagen'/>
            </div>
        </div>
         <div class='input-field col s12 m6'>
            <span>Estado:</span>
            <input id='activo' type='radio' name='estado' class='with-gap' value='1' <?php print(($estado == 1)?"checked":""); ?>/>
            <label for='activo'><i class='small material-icons left'>visibility</i></label>
            <input id='inactivo' type='radio' name='estado' class='with-gap' value='0' <?php print(($estado == 0)?"checked":""); ?>/>
            <label for='inactivo'><i class='small material-icons left'>visibility_off</i></label>
        </div>
    </div>
        <div class='input-field col s12 m6'>
            <?php
            $sql = "SELECT id_colores, nombre_color FROM colores";
            Page::setCombo("Color", "color", $color, $sql);
            ?>
        </div>
        <div class='input-field col s12 m6'>
            <?php
            $sql = "SELECT id_tipo_producto, nombre_tipo_producto FROM tipo_producto";
            Page::setCombo("Tipo", "tipo", $tipo, $sql);
            ?>
        </div>
        <div class='input-field col s12 m6'>
            <?php
            $sql = "SELECT id_talla, talla FROM talla";
            Page::setCombo("Talla", "talla", $talla, $sql);
            ?>
        </div>
    <div class='row center-align'>
        <a href='index.php' class='btn waves-effect grey'><i class='material-icons'>cancel</i></a>
        <button type='submit' class='btn waves-effect blue'><i class='material-icons'>save</i></button>
    </div>
</form>

<?php
Page::footer();
?>