<!-- Se solicita el archivo maestro donde esta la configuracion de la pagina -->
<?php
require("../page/page.php");
//Se coloca el titulo
Page::header("Productos");

//Se utiliza una consulta para realizar el buscador
if(!empty($_POST))
{
	$search = trim($_POST['buscar']);
	$sql = "SELECT * FROM productos, colores WHERE productos.id_colores = colores.id_colores AND colores LIKE ? ORDER BY nombre_color";
	$sql = "SELECT * FROM productos, tipo_producto WHERE productos.id_tipo_producto = tipo_producto.id_tipo_producto AND nombre_tipo_producto LIKE ? ORDER BY nombre_tipo_producto";
	$sql = "SELECT * FROM productos, talla WHERE productos.id_talla = talla.id_talla AND talla LIKE ? ORDER BY talla";
	$params = array("%$search%", "%$search%");
}
else
{
	$sql = "SELECT * FROM productos, colores WHERE productos.id_colores = colores.id_colores ORDER BY nombre_color";
	$sql = "SELECT * FROM productos, tipo_producto WHERE productos.id_tipo_producto = tipo_producto.id_tipo_producto ORDER BY nombre_tipo_producto";
	$sql = "SELECT * FROM productos, talla WHERE productos.id_talla = talla.id_talla ORDER BY talla";
	$params = null;
}
$data = Database::getRows($sql, $params);
if($data != null)
{
?>
<!-- Los botones y los cuadros de texto-->
<form method='post'>
	<div class='row'>
		<div class='input-field col s6 m4'>
			<i class='material-icons prefix'>search</i>
			<input id='buscar' type='text' name='buscar'/>
			<label for='buscar'>Buscar</label>
		</div>
		<div class='input-field col s6 m4'>
			<button type='submit' class='btn waves-effect green'><i class='material-icons'>check_circle</i></button> 	
		</div>
		<div class='input-field col s12 m4'>
			<a href='guardar.php' class='btn waves-effect indigo'><i class='material-icons'>add_circle</i></a>
		</div>
	</div>
</form>
<!-- Titulos de las columnas de la tabla-->
<table class='striped'>
	<thead>
		<tr>
		    
			<th>IMAGEN</th>
			<th>NOMBRE</th>
			<th>DESCRIPCION</th>
			<th>ESTADO</th>
			<th>COLOR</th>
			<th>TIPO</th>
			<th>TALLA</th>
			<th>ACCIÓN</th>
		</tr>
	</thead>
	<tbody>

<!--Se mandan a llamar los datos de la base-->
<?php
	foreach($data as $row)
	{
		print("
			<tr>
				<td><img src='data:image/*;base64,".$row['imagen']."' class='materialboxed' width='100' height='100'></td>
				<td>".$row['nombre_producto']."</td>
				<td>".$row['descripcion_producto']."</td>
				<td>".$row['estado_producto']."</td>
				<td>".$row['id_colores']."</td>
				<td>".$row['id_tipo_producto']."</td>
				<td>".$row['id_talla']."</td>
				<td>
				    <a href='guardar.php?id=".$row['id_producto']."' class='blue-text'><i class='small material-icons'>edit</i></a>
					<a href='eliminar.php?id=".$row['id_producto']."' class='red-text'><i class='small material-icons'>delete</i></a>
				</td>
		   </tr>
		");
		if($row['estado_producto'] == 1)
		{
			print("<i class='small material-icons'>visibility</i>");
		}
		else
		{
			print("<i class='small material-icons'>visibility_off</i>");
		}
		
	}
	print("
		</tbody>
	</table>
	");
} //Fin de if que comprueba la existencia de registros.
else
{
	Page::showMessage(4, "No hay registros disponibles", "guardar.php");
}
Page::footer();
?>