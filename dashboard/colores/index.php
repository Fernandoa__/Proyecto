<!-- Se solicita el archivo maestro donde esta la configuracion de la pagina -->
<?php
require("../page/page.php");
// Se coloca el titulo
Page::header("Colores");

if(!empty($_POST))
{
	//Se utiliza una consulta para realizar el buscador
	$search = trim($_POST['buscar']);
	$sql = "SELECT * FROM colores WHERE nombre_color LIKE ? ORDER BY id_color";
	$params = array("%$search%");
}
else
{
	$sql = "SELECT * FROM colores ORDER BY id_color";
	$params = null;
}
$data = Database::getRows($sql, $params);
if($data != null)
{
?>
<!-- Los botones y los cuadros de texto-->
<form method='post'>
	<div class='row'>
		<div class='input-field col s6 m4'>
			<i class='material-icons prefix'>search</i>
			<input id='buscar' type='text' name='buscar'/>
			<label for='buscar'>Buscar</label>
		</div>
		<div class='input-field col s6 m4'>
			<button type='submit' class='btn waves-effect green'><i class='material-icons'>check_circle</i></button> 	
		</div>
		<div class='input-field col s12 m4'>
			<a href='guardar.php' class='btn waves-effect indigo'><i class='material-icons'>add_circle</i></a>
		</div>
	</div>
</form>
<!-- Titulos de las columnas de la tabla-->
<table class='striped'>
	<thead>
		<tr>
			<th>NOMBRE</th>
			<th>ACCIÓN</th>
		</tr>
	</thead>
	<tbody>

<!--Se mandan a llamar los datos de la base-->
<?php
	foreach($data as $row)
	{
		print("
			<tr>
				<td>".$row['nombre_color']."</td>
				<td>
					<a href='guaradar.php?id=".$row['id_tipo_producto']."' class='blue-text'><i class='small material-icons'>edit</i></a>
					<a href='eliminar.php?id=".$row['id_tipo_producto']."' class='red-text'><i class='small material-icons'>delete</i></a>
				</td>
			</tr>
		");
	}
	print("
		</tbody>
	</table>
	");
} //Fin de if que comprueba la existencia de registros.
else
{
	Page::showMessage(4, "No hay registros disponibles", "guardar.php");
}
Page::footer();
?>