<!-- Se solicita el archivo maestro donde esta la configuracion de la pagina -->
<?php
require("../page/page.php");
if(empty($_GET['id'])) 
{
    Page::header("Agregar colores");
    //Se declaran la variables
    $id = null;
    $nombre = null;
}
else
{
    Page::header("Modificar colores");
    //Se obtiene el Id del dato guardado
    $id = $_GET['id'];
    $sql = "SELECT * FROM colores WHERE id_colores = ?";
    $params = array($id);
    //Se obtienen los datos de todas la columnas
    $data = Database::getRow($sql, $params);
    $nombre = $data['nombre_color'];
}

if(!empty($_POST))
{
    //Se validan los datos
    $_POST = Validator::validateForm($_POST);
  	$nombre = $_POST['nombre_color'];
    try 
    {
        //Si los campos estan vacios no se permitira que se guarden
      	if($nombre != "")
        {
            if($id == null)
            {
                $sql = "INSERT INTO colores(nombre_color) VALUES(?)";
                $params = array($nombre);
            }
            else
            {
                $sql = "UPDATE colores SET colores = ? WHERE nombre_color = ?";
                $params = array($nombre, $id);
            }
            Database::executeRow($sql, $params);
            header("location: index.php");
        }
        else
        {
            throw new Exception("Debe digitar un nombre");
        }
    }
    catch (Exception $error)
    {
        Page::showMessage(2, $error->getMessage(), null);
    }
}
?>

<!-- Botones y cuadros de texto -->
<form method='post'>
    <div class='row'>
        <div class='input-field col s12 m12'>
            <i class='material-icons prefix'>note_add</i>
            <input id='nombre' type='text' name='nombre' class='validate' value='<?php print($nombre); ?>' required/>
            <label for='nombre'>Nombre</label>
        </div>
    </div>
    <div class='row center-align'>
        <a href='index.php' class='btn waves-effect grey'><i class='material-icons'>cancel</i></a>
        <button type='submit' class='btn waves-effect blue'><i class='material-icons'>save</i></button>
    </div>
</form>

<?php
Page::footer();
?>