<?php 
require("../lib/database.php");
require("../lib/validator.php");

#Funcion que sirve para validar la contraseña, donde se validara longitud de la clave, si tiene mayusculas y minusculas.
function validar_clave($clave,&$error_clave){
   if(strlen($clave) < 8){
      $error_clave = "La clave debe tener al menos 8 caracteres";
      return false;
   }
   if(strlen($clave) > 16){
      $error_clave = "La clave no puede tener más de 16 caracteres";
      return false;
   }
   if (!preg_match('`[a-z]`',$clave)){
      $error_clave = "La clave debe tener al menos una letra minúscula";
      return false;
   }
   if (!preg_match('`[A-Z]`',$clave)){
      $error_clave = "La clave debe tener al menos una letra mayúscula";
      return false;
   }
   if (!preg_match('`[0-9]`',$clave)){
      $error_clave = "La clave debe tener al menos un caracter numérico";
      return false;
   }
   $error_clave = "";
   return true;
} 

$imagen = null;
if(!empty($_POST))
{
    $_POST = Validator::validateForm($_POST);
    $nombres = $_POST['nombre_cliente'];
  	$apellidos = $_POST['apellido_cliente'];
    $correo = $_POST['correo'];
    $alias = $_POST['alias'];
    $clave1 = $_POST['clave1'];
    $clave2 = $_POST['clave2'];
    $archivo = $_FILES['foto'];

    if($archivo['name'] != null)
    {
        $base64 = Validator::validateImage($archivo);
        if($base64 != false)
        {
            $imagen = $base64;
        }
        else
        {
            throw new Exception("Ocurrió un problema con la imagen");
        }
    }
    else
    { 
        #imagen predefinida
        if($imagen == null)
        {
            $imagen = Validator::imagen_usuario();
        }
    }

  #Expresion regular que sirve para validar nombres y apellidos que en las iniciales de cada nombre tenga espacios y no tenga numeros.
  $nombre = "/^([A-ZÁÉÍÓÚ]{1}[a-zñáéíóú]+[\s]*)+$/";
  try 
    {
        #Se valida los espacios vacios de cada campo.
      	if($nombres != "" && $apellidos != "")
        {
            #Se valida aqui con el campo nombre y la expresion regular anteriormente hecha.
            if (preg_match($nombre, $_POST['nombre_cliente'])) {
                #Se valida aqui con el campo apellido y la expresion regular anteriormente hecha.
                if (preg_match($nombre, $_POST['apellido_cliente'])) {
                    if($correo != "")
                    {
                        #Se valida el correo.
                        if (filter_var($_POST['correo'], FILTER_VALIDATE_EMAIL)) {
                            $error_encontrado="";
                            if (validar_clave($_POST['clave1'], $error_encontrado)){
                                if($clave1 != "" && $clave2 != "")
                                {
                                    #Se valida que las dos claves sean iguales.
                                    if($clave1 == $clave2)
                                    {
                                        #Se encripta la clave con paswrod hash
                                        $clave = password_hash($clave1, PASSWORD_DEFAULT);
                                        $sql = "INSERT INTO cliente( nombre_cliente, apellido_cliente, correo, alias, clave, foto) VALUES (?,?,?,?,?,?)";
                                        $params = array($nombres, $apellidos, $correo, $alias, $clave,$imagen);
                                        Database::executeRow($sql, $params);
                                        header("location: iniciar_sesion.php");
                                    }
                                    else
                                    {
                                        throw new Exception("Las contraseñas no coinciden");
                                    }
                                }
                                else
                                {
                                    throw new Exception("Debe ingresar ambas contraseñas");
                                }
                            }else{
                                throw new Exception($error_encontrado);
                            }
                        }else{
                            throw new Exception("Verifique el correo!");
                        }
                    }
                    else
                    {
                        throw new Exception("Debe ingresar un correo electrónico");
                    }
            }else{
                    throw new Exception("Verifique el apellido!");
                }
            }else{
                throw new Exception("Verifique el nombre!");
            }
        }
        else
        {
            throw new Exception("Debe ingresar el nombre completo");
        }
    }
    catch (Exception $error)
  {
        
    }
}
?>


<!DOCTYPE html>
        <html lang="es">
        <head>
        <title>Inicio | Ariarti</title>
        <meta charset="utf-8"/>
        <link rel="stylesheet" href="../css/materialize.min.css">
        <link href="../css/miestilo.css" type="text/css" rel="stylesheet" media="screen,projection"/>
        <link rel='stylesheet' type='text/css' href='../css/icons.css'>

        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

   <?php 
         include("../lib/menu.php"); 
     ?>
  

<form class="container" method="post" enctype='multipart/form-data'>
    <div class="row">
        <h4 class="center-align">Registrarse</h4>
    </div>
    <div class='file-field input-field col s12 m6'>
            <div class='btn waves-effect green darken-2'>
                <span><i class='material-icons'>image</i></span>
                <input type='file' name='imagen' <?php print(($imagen == null)?:""); ?>/>
            </div>
            <div class='file-path-wrapper'>
                <input class='file-path validate' type='text' placeholder='Seleccione una imagen'/>
            </div>
      </div>
    <div class='row'>
        <div class='input-field col s12 m6'>
          	<i class='material-icons prefix'>person</i>
          	<input id='nombres' type='text' name='nombre' class='validate'>
          	<label for='nombres'>Nombres</label>
        </div>
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>person</i>
            <input id='apellidos' type='text' name='apellido' class='validate'>
            <label for='apellidos'>Apellidos</label>
        </div>
        <div class='input-field col s12 m12'>
            <i class='material-icons prefix'>email</i>
            <input id='correo' type='email' name='correo' class='validate' >
            <label for='correo'>Correo</label>
        </div>
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>security</i>
            <input id='clave1' type='password' name='clave1' class='validate' >
            <label for='clave1'>Contraseña</label>
        </div>
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>security</i>
            <input id='clave2' type='password' name='clave2' class='validate' >
            <label for='clave2'>Confirmar contraseña</label>
        </div>
    </div>
    <div class='row center-align'>
 	    <button type='submit' class='btn waves-effect red darken-4'>Aceptar<i class='material-icons left'>send</i></button>
    </div>
    <br>
</form>
<?php 
         include("../lib/footer.php"); 
     ?>
  <!-- Importamos el JQuery de materilize  -->
        <script src="../js/jquery.js"></script>
        <script src="../js/materialize.min.js"></script>
        <script type="text/javascript" src="../js/main.js"></script>
        </body>
        </html>

      