 <!DOCTYPE html>
  <html lang ='es'>
 <!DOCTYPE html>
  <html lang ='es'>
   <head>
    <meta charset='utf-8'>
      <link type='text/css' rel='stylesheet' href='../css/materialize.min.css'/>
	    <link type='text/css' rel='stylesheet' href='../css/icons.css'/>
      <link href="../css/miestilo.css" type="text/css" rel="stylesheet" media="screen,projection"/>
	    <meta name='viewport' content='width=device-width, initial-scale=1.0'/>
   </head>
 <body class= "grey lighten-5">
  
    <?php
        include("../lib/menu.php") 
    ?>
    <h4 class="center red-text text-lighten-1"><b>Nosotros</b></h4>
  <div class="container">
  <ul class="collapsible" data-collapsible="accordion">
    <li>
      <div class="collapsible-header"><i class="material-icons">trending_up</i><b>Mision</b></div>
      <div class="collapsible-body"><span class="red-text">Ser la mayor empresa de confección y promocionales en la región centroamericana que ofrezca la mejor calidad y variedad de productos y servicios.
       Generar un valor agregado a nuestros clientes, fortaleciendo nuestra solidez por medio de la planeacion y trabajo en equipo.
    </span></div>
    </li>
    <li>
      <div class="collapsible-header"><i class="material-icons">visibility</i><b>Vision</b></div>
      <div class="collapsible-body"><span  class="red-text">Somos empresa líder en el área de confección de prendas de vestir y artículos promocionales, orientada a ofrecer la mejor calidad y 
      variedad en productos y servicios, brindándole a nuestros clientes las mejores opciones de compra, obteniendo de esta manera su confianza y lealtad
</span></div>
    </li>
    <li>
      <div class="collapsible-header"><i class="material-icons">thumb_up</i><b>Valores</b></div>
      <div class="collapsible-body"><span class="red-text"> 
      Respeto,
      confianza,
      Lealtad,
      trabajo en equipo,
      Calidad en servicios.</span></div>
    </li>
  </ul>
</div>

   <div class="parallax-container">
      <div class="parallax"><img src="../img/f.jpg"></div>
   </div>

<h4 class="center red-text text-lighten-1"><b>FAQ</b></h4>
  <div class="container">
  <ul class="collapsible" data-collapsible="accordion">
    <li>
      <div class="collapsible-header"><i class="material-icons">place</i><b>¿Donde estamos ubicado?</b></div>
      <div class="collapsible-body"><span>Estamos ubicados en colonia Miralvalle Avenida Florencia No 40, San Salvador.</span></div>
    </li>
    <li>
      <div class="collapsible-header"><i class="material-icons">place</i><b>¿En que horarios puedes visitarnos?</b></div>
      <div class="collapsible-body"><span>Visitanos de lunes a viernes de 8 de la mañana a 4 de la tarde y los dias sabados de 8 de la mañana a 12 del medio dia.</span></div>
    </li>
    <li>
      <div class="collapsible-header"><i class="material-icons">whatshot</i><b>¿Que tipo de productos fabricamos?</b></div>
      <div class="collapsible-body"><span>Tenemos una amplia gama de productos, desde camisetas, uniformes, gabachas, hasta promosionales, enterate mas de lo que ofrecemos en la seccion de productos de este mismo sitio web.</span></div>
    </li>
        <li>
      <div class="collapsible-header"><i class="material-icons">whatshot</i><b>¿Para que tipo de publico van dirigidos nuestros productos?</b></div>
      <div class="collapsible-body"><span>Nosotros fabricamos productos que se apegan a las necesidades de cualquier persona, esto incluye a mujeres y hombres.</span></div>
    </li>
    <li>
      <div class="collapsible-header"><i class="material-icons">whatshot</i><b>¿Como puedes adquirir nuestros productos?</b></div>
      <div class="collapsible-body"><span>Puedes adquirir nuestros productos contactandonos o visitandonos, recuerda que trabajamos con pedidos, si tu lo que necesitas es la produccion de alguno de nuestros productos en general.</span></div>
    </li>
    <li>
      <div class="collapsible-header"><i class="material-icons">whatshot</i><b>¿Como cotizar un estimado de un pedido?</b></div>
      <div class="collapsible-body"><span>Puedes contactarnos para que te brindemos un dato precizo segun lo que necesites, o mas bien puedes registrarte en nuestro sistema y en el puedes ejecutar estimados de tu cotizacion.</span></div>
    </li>
    <li>
      <div class="collapsible-header"><i class="material-icons">whatshot</i><b>¿Cual es el minimo de productos por encargo que realizamos?</b></div>
      <div class="collapsible-body"><span>Esto dependera del tipo de producto, por lo general se vende por docena.</span></div>
    </li>
    <li>
      <div class="collapsible-header"><i class="material-icons">whatshot</i><b>¿Como te puedes poner en contacto con nosotros?</b></div>
      <div class="collapsible-body"><span>Lorem ipsum dolor sit amet.</span></div>
    </li>
  </ul>
  </div>

    <div class="parallax-container">
      <div class="parallax"><img src="../img/f.jpg"></div>
    </div>
    
      <?php
        include("../lib/footer.php") 
    ?>

      <!--Import jQuery before materialize.js-->
      <script src="../js/jquery-2.1.1.min.js"></script>
      <script src="../js/materialize.min.js"></script>
      <script src="../js/public.js"></script>

    </body>
  </html>