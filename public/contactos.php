 <!DOCTYPE html>
  <html lang ='es'>
   <head>
    <meta charset='utf-8'>
      <link type='text/css' rel='stylesheet' href='../css/materialize.min.css'/>
	    <link type='text/css' rel='stylesheet' href='../css/icons.css'/>
      <link href="../css/miestilo.css" type="text/css" rel="stylesheet" media="screen,projection"/>
	    <meta name='viewport' content='width=device-width, initial-scale=1.0'/>
   </head>
 <body>

    <!--menu-->    
    <?php
        include("../lib/menu.php") 
    ?>

      <!--Card 1 para enviar correos-->
      <div class="container">
        <div class="row">
         <h4 class="center-align red-text text-lighten-1"><b>Envianos tu correo</b></h4>
          <form>
              <div class="input-field col s12 m6">
                <input id="nombre" type="text" class="validate">
                <label for="nombre">Nombre</label>
              </div>
              <div class="input-field col s12 m6">
                <input id="apellido" type="text" class="validate">
                <label for="apellido">Apellido</label>
              </div>
              <div class="input-field col s12">
                <input id="asunto" type="text" class="validate">
                <label for="asunto">Asunto</label>
              </div>
            <p class="center-align"><a class="waves-effect waves-light btn teal lighten-2"><i class="material-icons right">navigation</i>Enviar</a></p>
          </form>
          </div>
        </div>

<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3875.9512834251964!2d-89.22394688882771!3d13.721399293025746!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8f633071591e34e7%3A0x4364f6c59717ce32!2sAvenida+Florencia%2C+San+Salvador!5e0!3m2!1ses-419!2ssv!4v1489792917123" width="1345" height="500" frameborder="0" style="border:0" allowfullscreen></iframe>

 <!--Import jQuery before materialize.js-->
      <script src="../js/jquery-2.1.1.min.js"></script>
      <script src="../js/materialize.min.js"></script>
      <script src="../js/public.js"></script>

      <?php
          include("../lib/footer.php") 
      ?>
    </body>
  </html>








