 <!DOCTYPE html>
  <html lang ='es'>
   <head>
    <meta charset='utf-8'>
      <link type='text/css' rel='stylesheet' href='../css/materialize.min.css'/>
	    <link type='text/css' rel='stylesheet' href='../css/icons.css'/>
      <link href="../css/miestilo.css" type="text/css" rel="stylesheet" media="screen,projection"/>
	    <meta name='viewport' content='width=device-width, initial-scale=1.0'/>
   </head>
 <body>
  
    <?php
        include("../lib/menu.php") 
    ?>
     <!-- Media para las imagenes del index -->
  
  <div class="slider">
    <ul class="slides">
      <li>
        <img src="../img/slider/1.png"> <!-- random image -->
        <div class="caption center-align">
          <h3>This is our big Tagline!</h3>
          <h5 class="light grey-text text-lighten-3">Here's our small slogan.</h5>
        </div>
      </li>
      <li>
        <img src="../img/slider/2.png"> <!-- random image -->
        <div class="caption left-align">
          <h3>Left Aligned Caption</h3>
          <h5 class="light grey-text text-lighten-3">Here's our small slogan.</h5>
        </div>
      </li>
      <li>
        <img src="../img/slider/3.png"> <!-- random image -->
        <div class="caption right-align">
          <h3>Right Aligned Caption</h3>
          <h5 class="light grey-text text-lighten-3">Here's our small slogan.</h5>
        </div>
      </li>
      <li>
        <img src="../img/slider/4.png"> <!-- random image -->
        <div class="caption center-align">
          <h3>This is our big Tagline!</h3>
          <h5 class="light grey-text text-lighten-3">Here's our small slogan.</h5>
        </div>
      </li>
    </ul>
  </div>

 <!-- Cards de informacion-->
 <h4 class="center">Ariarti es ...</h4>
    <div class="row">
      <div class="col s4 ">
        <div class="card-panel white">
        <center><i class=" large material-icons pink-text">invert_colors</i><center>
          <span class="pink-text"> Vive la intensidad de los colores en nuestras impreciones.
          </span>
        </div>
      </div>
<div class="col s4 ">
        <div class="card-panel white">
        <center><i class=" large material-icons pink-text">invert_colors</i><center>
          <span class="pink-text"> Vive la intensidad de los colores en nuestras impreciones.
          </span>
        </div>
      </div>
<div class="col s4 ">
        <div class="card-panel white">
        <center><i class=" large material-icons pink-text">invert_colors</i><center>
          <span class="pink-text"> Vive la intensidad de los colores en nuestras impreciones.
          </span>
        </div>
      </div>
   </div>
       
    <div class="parallax-container">
      <div class="parallax"><img src="../img/f.jpg"></div>
    </div>
             
 


      <!--Import jQuery before materialize.js-->
      <script src="../js/jquery-2.1.1.min.js"></script>
      <script src="../js/materialize.min.js"></script>
      <script src="../js/public.js"></script>
      <?php
          include("../lib/footer.php") 
      ?>
    </body>
  </html>