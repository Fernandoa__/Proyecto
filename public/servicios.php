 <!DOCTYPE html>
  <html lang ='es'>
   <head>
    <meta charset='utf-8'>
      <link type='text/css' rel='stylesheet' href='../css/materialize.min.css'/>
	    <link type='text/css' rel='stylesheet' href='../css/icons.css'/>
      <link href="../css/miestilo.css" type="text/css" rel="stylesheet" media="screen,projection"/>
	    <meta name='viewport' content='width=device-width, initial-scale=1.0'/>
   </head>
 <body>

    <?php
        include("../lib/menu.php") 
    ?>
     
     <div class="video-container">
        <iframe width="600" height="200" src="../img/VIDEO.webm" frameborder="0" allowfullscreen></iframe>
      </div>

    <!--Cards de servicios-->   
     <div class="container">
   <div class="col s12 m7">
    <h4 class="header">Serigrafia</h4>
    <div class="card horizontal">
      <div class="card-image">
        <img src="../img/h.jpg">
      </div>
      <div class="card-stacked">
        <div class="card-content">
            <h5>¿Que es?</h5>
          <p>La serigrafía es una técnica de reproducción de imágenes que consiste en hacer pasar tinta a través de un tejido técnico montado en un bastidores de aluminio o madera que constituye el molde de impresión. </p>
            <h5>Impresion</h5>
            <p>La impresión se realizará con las pantallas a través de los huecos libres de emulsión arrastrando las tintas manualmente con una rasqueta de caucho. La tinta se deposita sobre el tejido. cuando los formatos de la impresión son muy grandes resulta muy complicado hacerlo manualmente porque no es posible mantener el mismo nivel de presión en toda el área de impresión por lo que se hace necesario el uso de una maquina que cumpla esa función de arrastre de la rasqueta sin que esto le reste su carácter artesanal al producto final.</p>
        </div>
      </div>
    </div>
  </div>
        <div class="col s12 m7">
    <h4 class="header">Bordados</h4>
    <div class="card horizontal">
      <div class="card-image">
        <img src="../img/b.jpg">
      </div>
      <div class="card-stacked">
        <div class="card-content">
            <h5>¿Que es?</h5>
          <p>El bordado es el sistema de personalización más adecuado para añadir valor a vuestras prendas. Es una técnica muy resistente y de alta calidad que recomendamos principalmente para ropa laboral, clubs deportivos, o eventos de prestigio. Personalizamos camisetas, polos, sudaderas o gorras a partir de 10 unidades.</p>
            <h5>Impresion</h5>
            <p>La impresión se realizará con las pantallas a través de los huecos libres de emulsión arrastrando las tintas manualmente con una rasqueta de caucho. La tinta se deposita sobre el tejido. cuando los formatos de la impresión son muy grandes resulta muy complicado hacerlo manualmente porque no es posible mantener el mismo nivel de presión en toda el área de impresión por lo que se hace necesario el uso de una maquina que cumpla esa función de arrastre de la rasqueta sin que esto le reste su carácter artesanal al producto final.</p>
        </div>
      </div>
    </div>
  </div>
  
   <div class="col s12 m7">
    <h4 class="header">Sublimacion</h4>
    <div class="card horizontal">
      <div class="card-image">
        <img src="../img/s.jpg">
      </div>
      <div class="card-stacked">
        <div class="card-content">
            <h5>¿Que es?</h5>
          <p>Lo que llamamos sublimación textil es una técnica relativamente reciente de personalizar o imprimir prendas blancas de poliester 100%.
            Es el sistema de impresión ideal para las prendas técnicas y deportivas porque no tapa el poro del tejido y permite la transpiración</p>
            <h5>Recomendacion</h5>
            <p>
             Estamos equipados con máquinas de múltiples cabezales para bordar todo tipo de prendas hasta 15 colores. Por norma general solemos bordar logos sencillos de 1, 2 o 3 colores. 
             Recordad que esta técnica no permite reproducir diseños complicados y mucho menos fotografías.
        </div>
      </div>
    </div>
  </div>
  </div>

  <!--Parallax del final de pag--> 
 <div class="parallax-container">
      <div class="parallax"><img src="../img/para21.png"></div>
  </div>

 <!--Footer-->
     <?php
        include("../lib/footer.php") 
    ?>

<!--Import jQuery before materialize.js-->
      <script src="../js/jquery-2.1.1.min.js"></script>
      <script src="../js/materialize.min.js"></script>
      <script src="../js/public.js"></script>

   </body>
</html>