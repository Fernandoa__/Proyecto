<?php
  require("../lib/database.php");

  if(!empty($_POST))
  {
      $alias = htmlspecialchars($_POST['usuario']);
      $clave = htmlspecialchars($_POST['clave']);
      try
      {
        if($alias != "" && $clave != "")
        {
            $sql = "SELECT * FROM cliente WHERE correo = ?";
            $param = array($alias);
            $data = Database::getRow($sql, $param);
            $hash = $data['clave'];
            if($data != null)
            {
               if(password_verify($clave, $hash)) 
               {      
                    session_start();
                    $_SESSION['id'] = $data['id_cliente'];
                    header("location: index.php");
               }else{
                   throw new Exception("La clave ingresada es incorrecta.");
                }
          
            }else{
              throw new Exception("El alias ingresado no existe.");
            }
        }else{
          throw new Exception("Debe ingresar un alias y una clave.");
        }
      }
      catch (Exception $error)
      {
          print("<div class='card-panel red white-text'><i class='material-icons left'>error</i>".$error->getMessage()."</div>");
      }
  }
?>
 <!DOCTYPE html>
  <html lang ='es'>
   <head>
    <meta charset='utf-8'>
      <link type='text/css' rel='stylesheet' href='../css/materialize.min.css'/>
	    <link type='text/css' rel='stylesheet' href='../css/icons.css'/>
      <link href="../css/miestilo.css" type="text/css" rel="stylesheet" media="screen,projection"/>
	    <meta name='viewport' content='width=device-width, initial-scale=1.0'/>
   </head>
 <body>

    <?php
        include("../lib/menu.php") 
    ?>
 <br>
    <form method="post">
    <div class="row">
        <div class="col s6 offset-s3">
          <div class="row">
            <div class="col s12 center-align">
              <h5>Acceder a mi cuenta</h5>
            </div>        
          </div>
          <div class="row">
            <div class="input-field col s12">
              <i class="material-icons prefix">account_circle</i>
              <input id="icon_prefix" type="text" class="validate" name="usuario" autocomplete="off">
              <label for="icon_prefix">Usuario</label>
            </div>
          </div>
          <div class="row">
            <div class="input-field col s12">
              <i class="material-icons prefix">security</i>
              <input id="icon_telephone" type="password" class="validate" name="clave">
              <label for="icon_telephone">Contraseña</label>
            </div>
          </div>
          <div class="row">
            <div class="col s12 center-align">
              <button class="waves-effect waves-light btn red darken-4" type="submit">Iniciar Sesion </button>
              <br>
              <br>
              <a href="registrar.php">Registrate por primera vez aqui</a>
            </div>        
          </div>
        </div>      
    </div>
      
    </form>
<br>

     <?php 
         include("../lib/footer.php"); 
     ?>
        <!-- Importamos el JQuery de materilize  -->
        <script src="../../js/jquery.js"></script>
        <script src="../../js/materialize.min.js"></script>
        <script src="../../js/main.js"></script>
        </body>
        </html>