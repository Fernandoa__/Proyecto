  <!DOCTYPE html>
  <html lang ='es'>
   <head>
    <meta charset='utf-8'>
      <link type='text/css' rel='stylesheet' href='../css/materialize.min.css'/>
	    <link type='text/css' rel='stylesheet' href='../css/icons.css'/>
      <link href="../css/miestilo.css" type="text/css" rel="stylesheet" media="screen,projection"/>
	    <meta name='viewport' content='width=device-width, initial-scale=1.0'/>
   </head>
 <body>
    
    <?php
        include("../lib/menu.php") 
    ?>

<!-- Cards primer nivel -->
  <h4 class="center red-text"><b>Catalogo<b></h4>
    <div class="container">
        <div class="row">
        <!---se mandan a llamar los productos para encapsularlos en un card-->
            <?php
		require("../lib/database.php");  
         $sql = "SELECT * FROM productos, tipo_producto WHERE productos.id_tipo_producto = tipo_producto.id_tipo_producto AND estado_producto = 1";
		     $data = Database::getRows($sql, null);
		     if($data != null)
		     {
			    foreach ($data as $row) 
		    {
				 print("        
          <div class='col s12 m4 push-m2'>
            <div class='card'>
             <div class='card-image'>
              <img  src='data:image/*;base64,$row[imagen]'>
              <span class='card-title red-text'>$row[nombre_producto]</span>
              <a href='camisas.php' class='btn-floating halfway-fab waves-effect waves-light grey'><i class='material-icons'>add</i></a>
            </div>
         <div class='card-content'>
          <p>$row[descripcion_producto]</p>
        </div>
       </div>
       </div>
       ");
			}
		}
		else
		{
			print("<div class='card-panel yellow'><i class='material-icons left'>warning</i>No hay productos disponibles en este momento.</div>");
		}
		?>
		</div><!-- Fin de row -->
	</div><!-- Fin de container -->
          <!--Import jQuery before materialize.js-->
      <script src="../js/jquery-2.1.1.min.js"></script>
      <script src="../js/materialize.min.js"></script>
      <script src="../js/main.js"></script>
   <?php include ("../lib/footer.php");?>  
    </body>
</html>